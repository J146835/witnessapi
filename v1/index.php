<?php

require_once '../include/DbHandler.php';
require_once '../include/PassHash.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

// User id from db - Global Variable
$user_id = NULL;

/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 *
 * WORKING?
 */
function authenticate(\Slim\Route $route) {
    // Getting request headers
    $headers = apache_request_headers();
    $response = array();
    $app = \Slim\Slim::getInstance();

    // Verifying Authorization Header
    if (isset($headers['Authorization'])) {
        $db = new DbHandler();

        // get the api key
        $token = $headers['Authorization'];
        // validating api key
        if (!$db->isValidApiKey($token)) {
            // api key is not present in users table
            $response["error"] = true;
            $response["message"] = "Access Denied. Invalid Api key";
            echoRespnse(401, $response);
            $app->stop();
        } else {
            global $user_id;
            // get user primary key id
            $user_id = $db->getUserId($token);
        }
    } else {
        // api key is missing in header
        $response["error"] = true;
        $response["message"] = "Api key is misssing";
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * ----------- METHODS WITHOUT AUTHENTICATION ---------------------------------
 */
/**
 * User Registration
 * url - /register
 * method - POST
 * params - email
 *
 * WORKING
 */
$app->post('/register', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('email'));

            $response = array();

            // reading post params
            $email = $app->request->post('email');

            // validating email address
            validateEmail($email);

            $db = new DbHandler();
            $res = $db->createUser($email);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                $response["error"] = false;
                $response["message"] = "You are successfully registered";
            } else if ($res == USER_CREATE_FAILED) {
                $response["error"] = true;
                $response["message"] = "Oops! An error occurred while registereing";
            } else if ($res == USER_ALREADY_EXISTED) {
                $response["error"] = true;
                $response["message"] = "Sorry, this email already existed";
            }
            // echo json response
            echoRespnse(201, $response);
        });

/**
 * User Login
 * url - /login
 * method - POST
 * params - email, password
 *
 * WORKING
 */
$app->post('/login', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('email'));

            // reading post params
            $email = $app->request()->post('email');
            $response = array();

            $db = new DbHandler();
            // check for correct email and password
            if ($db->checkLogin($email)) {
                // get the user by email
                $user = $db->getUserByEmail($email);

                if ($user != NULL) {
                    $response['email'] = $user['email'];
                    $response['token'] = $user['token'];
                } else {
                    // unknown error occurred
                    $response['error'] = true;
                    $response['message'] = "An error occurred. Please try again";
                }
            } else {
                // user credentials are wrong
                $response['error'] = true;
                $response['message'] = 'Login failed. Incorrect credentials';
            }

            echoRespnse(200, $response);
        });

/*
 * ------------------------ METHODS WITH AUTHENTICATION ------------------------
 */

/* ------------- `incident` table methods ------------------ */

/**
 * Listing all incidents of the user
 * method GET
 * url /incidents
 */
$app->get('/incidents', 'authenticate', function() {
            global $user_id;
            $response = array();
            $db = new DbHandler();

            // fetching all user incidents
            $result = $db->getAllUserIncidents($user_id);

            $response["error"] = false;
            $response["incidents"] = array();

            // looping through result and preparing incidents array
            while ($incident = $result->fetch_assoc()) {
                $tmp = array();
                $tmp["id"] = $incident["id"];
                $tmp["description"] = $incident["description"];
                $tmp["latitude"] = $incident["latitude"];
                $tmp["longitude"] = $incident["longitude"];
                $tmp["created"] = $incident["created"];
                $tmp["appuser_id"] = $incident["appuser_id"];
                array_push($response["incidents"], $tmp);
            }

            echoRespnse(200, $response);
        });

/**
 * Listing single incident of the user
 * method GET
 * url /incidents/:id
 * Will return 404 if the incident doesn't belongs to user
 */
$app->get('/incidents/:id', 'authenticate', function($incident_id) {
            global $user_id;
            $response = array();
            $db = new DbHandler();

            // fetch incident
            $result = $db->getIncident($incident_id, $user_id);

            if ($result != NULL) {
                $response["error"] = false;
                $response["id"] = $result["id"];
                $response["description"] = $result["description"];
                $response["latitude"] = $result["latitude"];
                $response["longitude"] = $result["longitude"];
                $response["created"] = $result["created"];
                $response["appuser_id"] = $result["appuser_id"];
                echoRespnse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = "The requested resource doesn't exists";
                echoRespnse(404, $response);
            }
        });

/**
 * Creating new incident in db
 * method POST
 * params - name
 * url - /incidents/
 */
$app->post('/incidents', 'authenticate', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('description', 'latitude', 'longitude', 'appuser_id'));

            global $user_id;

            $response = array();
            $description = $app->request->post('description');
            $latitude = $app->request->post('latitude');
            $longitude = $app->request->post('longitude');
            $appuser_id = $app->request->post('appuser_id');

            $db = new DbHandler();

            // creating new incident
            $incident_id = $db->createIncident($description, $latitude, $longitude, $appuser_id);

            if ($incident_id != NULL) {
                $response["error"] = false;
                $response["message"] = "Incident created successfully";
                $response["incident_id"] = $incident_id;
                echoRespnse(201, $response);
            } else {
                $response["error"] = true;
                $response["message"] = "Failed to create incident. Please try again";
                echoRespnse(200, $response);
            }            
        });


/* ------------- `Metadata` table methods ------------------ */

/**
 * Listing all metadata of an incident
 * method GET
 * url /metadata/:id
 */
$app->get('/metadata/:id', 'authenticate', function($incident_id) {
    global $user_id;
    $response = array();
    $db = new DbHandler();

    // fetching all user incidents
    $result = $db->getAllIncidentsMetadata($incident_id);

    $response["error"] = false;
    $response["incidents"] = array();

    // looping through result and preparing incidents array
    while ($incident = $result->fetch_assoc()) {
        $tmp = array();
        $tmp["id"] = $incident["id"];
        $tmp["fileName"] = $incident["fileName"];
        $tmp["fileSize"] = $incident["fileSize"];
        $tmp["fileDateTime"] = $incident["fileDateTime"];
        $tmp["incident_id"] = $incident["incident_id"];
        array_push($response["incidents"], $tmp);
    }

    echoRespnse(200, $response);
});

/**
 * Creating new metadata in db
 * method POST
 * params - fileName, fileSize, fileDateTime, incident_id
 * url - /metadata
 */
$app->post('/metadata', 'authenticate', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('fileName', 'fileSize', 'fileDateTime', 'incident_id'));

    $response = array();
    $fileName = $app->request->post('fileName');
    $fileSize = $app->request->post('fileSize');
    $fileDateTime = $app->request->post('fileDateTime');
    $incident_id = $app->request->post('incident_id');

    $db = new DbHandler();

    // creating new incident
    $metadata = $db->createMetadata($fileName, $fileSize, $fileDateTime, $incident_id);

    if ($metadata != NULL) {
        $response["error"] = false;
        $response["message"] = "Metadata created successfully";
        $response["incident_id"] = $incident_id;
        echoRespnse(201, $response);
    } else {
        $response["error"] = true;
        $response["message"] = "Failed to create incident. Please try again";
        echoRespnse(200, $response);
    }
});

/* ------------- miscellaneous methods ------------------ */

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>
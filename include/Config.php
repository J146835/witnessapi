<?php
/**
 * Database configuration
 */
define('DB_USERNAME', 'witness_api');
define('DB_PASSWORD', 'secret');
define('DB_HOST', 'localhost');
define('DB_NAME', 'witness_api');

define('USER_CREATED_SUCCESSFULLY', 0);
define('USER_CREATE_FAILED', 1);
define('USER_ALREADY_EXISTED', 2);
?>

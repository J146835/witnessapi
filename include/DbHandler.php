<?php

/**
 * Class handles all db operations
 * This class contains CRUD methods for database tables
 */
class DbHandler
{

    private $conn;

    function __construct()
    {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    /* ------------- `users` table method ------------------ */

    /**
     * Creating new user
     * @param String $email User login email id
     *
     * WORKING
     */
    public function createUser($email)
    {
        $response = array();

        // First check if user already existed in db
        if (!$this->isUserExists($email)) {

            // Generating API key
            $token = $this->generateApiKey();

            // insert query
            $stmt = $this->conn->prepare("INSERT INTO appusers(email, token) values( ?, ?)");
            $stmt->bind_param("ss", $email, $token);

            $result = $stmt->execute();

            $stmt->close();

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            } else {
                // Failed to create user
                return USER_CREATE_FAILED;
            }
        } else {
            // User with same email already existed in the db
            return USER_ALREADY_EXISTED;
        }

        return $response;
    }

    /**
     * Checking user login
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     *
     * WORKING
     */
    public function checkLogin($email) {
        // fetching user by email
        $stmt = $this->conn->prepare("SELECT id FROM appusers WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->bind_result($user_id);

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // Found user with the email

            $stmt->fetch();

            $stmt->close();

            return TRUE;


        } else {
            $stmt->close();

            // user not existed with the email
            return FALSE;
        }
    }


    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     *
     * WORKING
     */
    private function isUserExists($email)
    {
        $stmt = $this->conn->prepare("SELECT id from appusers WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Fetching user by email
     * @param String $email User email id
     *
     * WORKING
     */
    public function getUserByEmail($email)
    {
        $stmt = $this->conn->prepare("SELECT email, token FROM appusers WHERE email = ?");
        $stmt->bind_param("s", $email);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($email, $token);
            $stmt->fetch();
            $user = array();
            $user["email"] = $email;
            $user["token"] = $token;
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user api key
     * @param String $user_id user id primary key in user table
     *
     * NOT TESTED, NOT USED?
     */
    public function getTokenById($user_id)
    {
        $stmt = $this->conn->prepare("SELECT token FROM appusers WHERE id = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            $stmt->bind_result($token);
            $stmt->close();
            return $token;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user id by api key
     * @param String $api_key user api key
     *
     * NOT TESTED
     */
    public function getUserId($token)
    {
        $stmt = $this->conn->prepare("SELECT id FROM appusers WHERE token = ?");
        $stmt->bind_param("s", $token);
        if ($stmt->execute()) {
            $stmt->bind_result($user_id);
            $stmt->fetch();
            $stmt->close();
            return $user_id;
        } else {
            return NULL;
        }
    }

    /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     *
     * NOT TESTED
     */
    public function isValidApiKey($token)
    {
        $stmt = $this->conn->prepare("SELECT id from appusers WHERE token = ?");
        $stmt->bind_param("s", $token);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Generating random Unique MD5 String for user Api key
     *
     * WORKING
     */
    private function generateApiKey()
    {
        return md5(uniqid(rand(), true));
    }

    /* ------------- `incidents` table method ------------------ */

    /**
     * Fetching all user incidents
     * @param String $user_id id of the user
     */
    public function getAllUserIncidents($appuser_id) {
        $stmt = $this->conn->prepare("SELECT * FROM incidents WHERE appuser_id = ?");
        $stmt->bind_param("i", $appuser_id);
        $stmt->execute();
        $incidents = $stmt->get_result();
        $stmt->close();
        return $incidents;
    }

    /**
     * Fetching single task
     * @param String $task_id id of the task
     */
    public function getIncident($incident_id, $user_id) {
        $stmt = $this->conn->prepare("SELECT * FROM incidents WHERE id = ? AND appuser_id  = ?");
        $stmt->bind_param("ii", $incident_id, $user_id);
        if ($stmt->execute()) {
            $res = array();
            $stmt->bind_result($id, $description, $latitude, $longitude, $created, $appuser_id);
            $stmt->fetch();
            $res["id"] = $id;
            $res["description"] = $description;
            $res["latitude"] = $latitude;
            $res["longitude"] = $longitude;
            $res["created"] = $created;
            $res["appuser_id"] = $appuser_id;
            $stmt->close();
            return $res;
        } else {
            return NULL;
        }
    }

    /**
     * Creates and incident record
     * @param $description
     * @param $latitude
     * @param $longitude
     * @param $user_id
     * @return null
     */
    public function createIncident($description, $latitude, $longitude, $user_id) {
        $stmt = $this->conn->prepare("INSERT INTO incidents(description, latitude, longitude, appuser_id) VALUES(?, ?,  ?, ?)");
        $stmt->bind_param("sddi", $description, $latitude, $longitude, $user_id );
        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            $new_task_id = $this->conn->insert_id;
            return $new_task_id;
        } else {
            // task failed to create
            return NULL;
        }
    }

    /* ------------- `metadata` table methods ------------------ */

    /**
     * Fetching all user incidents
     * @param String $incident_id id of the incident
     */
    public function getAllIncidentsMetadata($incident_id) {
        $stmt = $this->conn->prepare("SELECT * FROM metadata WHERE incident_id = ?");
        $stmt->bind_param("i", $incident_id);
        $stmt->execute();
        $metadata = $stmt->get_result();
        $stmt->close();
        return $metadata;
    }

    /**
     * Fetching single metadata
     * @param String $metadata_id id of the metadata
     *
     * NOT NEEDED?
     */
    public function getMetadata($metadata_id, $incident_id) {
        $stmt = $this->conn->prepare("SELECT * FROM metadata WHERE id = ? AND incident_id  = ?");
        $stmt->bind_param("ii", $metadata_id, $incident_id);
        if ($stmt->execute()) {
            $res = array();
            $stmt->bind_result($id, $fileName, $fileSize, $fileDateTime, $incident_id);
            $stmt->fetch();
            $res["id"] = $id;
            $res["fileName"] = $fileName;
            $res["fileSize"] = $fileSize;
            $res["fileDateTime"] = $fileDateTime;
            $res["incident_id"] = $incident_id;
            $stmt->close();
            return $res;
        } else {
            return NULL;
        }
    }

    /**
     * Creates an metadata record
     * @param $fileName
     * @param $fileSize
     * @param $fileDateTime
     * @param $incident_id
     * @return null
     */
    public function createMetadata($fileName, $fileSize, $fileDateTime, $incident_id) {
        $stmt = $this->conn->prepare("INSERT INTO metadata(fileName, fileSize, fileDateTime, incident_id) VALUES(?, ?,  ?, ?)");
        $stmt->bind_param("ssss", $fileName, $fileSize, $fileDateTime, $incident_id);
        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            $new_task_id = $this->conn->insert_id;
            return $new_task_id;
        } else {
            // task failed to create
            return NULL;
        }
    }
}
